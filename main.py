# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import argparse
import pandas as pd

from check_csv import check_csv
from check_matrix import check_matrix
from read_csv import read_csv
from check_rowcol import check_rowcol
from check_na import check_na
from check_type import check_type
from summary import summary
from show_results import show_results

def test():
    # コマンドラインからファイルのパスを受け取る
    parser = argparse.ArgumentParser(description='description')
    parser.add_argument('file', type=open, help='your file path')
    args = parser.parse_args()

    # ファイルのパス
    input_file = args.file

    # 拡張子が「.csv」かどうか
    is_csv = check_csv(input_file.name)

    # 行列形式かどうか（各行のカンマ数チェック）
    is_matrix = check_matrix(input_file.name)

    # CSVデータとして読み込み
    data = read_csv(input_file)

    # 行数、列数の取得
    rowcol_num = check_rowcol(data)

    # 各カラムのNAの数の取得
    na_nums = check_na(data)

    # 各カラムのデータ型の取得
    types = check_type(data)

    # 数値型のカラムの統計値の取得
    stat = summary(data)

    # 取得した情報の出力
    results = [is_csv, is_matrix, rowcol_num, na_nums, types, stat]
    show_results(results)

    return 0

if __name__ == "__main__":
    test()
