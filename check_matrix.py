
# argument
### data.csv

# return
### T or F

import csv
import sys

def check_matrix(filename):

    # import
    f = open(filename,'r')
    #print f

    # row count & making count box
    num_lines = sum(1 for line in open(filename))
    #print num_lines
    x = [0]*num_lines
    #print x

    # reading & checking
    reader = csv.reader(f)
    #print reader
    i = 0
    for row in reader:
	x[i] = row
	#print x[i]
	#print len(x[i])
	i = i + 1

    # return
    c = len(x[0])
    #print "head count = ", c
    for j in range(0+1,num_lines):
	if len(x[j]) != c:
	    print "Your data is not matrix"
	    #return False
	    sys.exit()
    #print "OK"
    return True

"""
### test

a = "a.csv"
#print a
print check_matrix(a)

print "=====|=====|====="

b = "b.csv"
#print b
print check_matrix(b)

print "=====|=====|====="

c = "c.csv"
#print c
print check_matrix(c)
"""

